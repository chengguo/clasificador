#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Desarrolla un programa que permita ingresar una cadena
(un string) y evalúe si es un correo electrónico, un entero,
un real u otra cosa.
"""

import sys


def es_correo_electronico(string):
    if '@' in string and '.' in string.split('@')[1]:
        return True
    else:
        return False


def es_entero(string):
    try:
        int(string)
        return True
    except ValueError:
        return False


def es_real(entrada):
    try:
        float(entrada)
        return True
    except ValueError:
        return False


def evaluar_entrada(string):
    if string == '':
        return None
    if es_correo_electronico(string):
        return (f"Es un correo electrónico.")
    if es_entero(string):
        return (f"Es un entero.")
    if es_real(string):
        return (f"Es un número real.")
    if string == 'texto':
        return (f"No es ni un correo, ni un entero, ni un número real.")


def main():
    if len(sys.argv) < 2:
        sys.exit("Error: se espera al menos un argumento")
    string = sys.argv[1]
    resultado = evaluar_entrada(string)
    print(resultado)


if __name__ == '__main__':
    main()
